-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2020 at 10:06 PM
-- Server version: 8.0.22
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopping`
--

-- --------------------------------------------------------

--
-- Table structure for table `bordures`
--

CREATE TABLE `bordures` (
  `id` bigint UNSIGNED NOT NULL,
  `material_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bordures`
--

INSERT INTO `bordures` (`id`, `material_id`, `name`, `picture`, `price`, `created_at`, `updated_at`) VALUES
(8, 8, 'chants-droit-adouci', 'chants-droit-adouci.jpg', 300, '2020-11-18 11:17:19', '2020-11-18 11:17:19'),
(11, 23, 'amit@gmail.com', 'profile-1606084613-137699.png', 1222, '2020-11-23 06:36:53', '2020-11-23 06:36:53');

-- --------------------------------------------------------

--
-- Table structure for table `coloris`
--

CREATE TABLE `coloris` (
  `id` bigint UNSIGNED NOT NULL,
  `matériaus_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coloris`
--

INSERT INTO `coloris` (`id`, `matériaus_id`, `name`, `picture`, `created_at`, `updated_at`) VALUES
(25, 18, 'Gc 03 - Ideal 0', 'profile-1604844015-452362.jpg', '2020-11-08 09:00:15', '2020-11-08 09:00:15'),
(26, 8, 'Alabama White0', 'profile-1604844489-795147.jpg', '2020-11-08 09:08:09', '2020-11-08 09:08:09'),
(32, 8, 'klklkl', 'profile-1605592345-946536.jpg', '2020-11-17 13:52:25', '2020-11-17 13:52:25'),
(33, 18, 'klklkl', 'profile-1605592454-49055.png', '2020-11-17 13:54:14', '2020-11-22 08:02:09'),
(34, 23, 'fghfgh', 'profile-1605618354-772939.png', '2020-11-17 21:05:54', '2020-11-17 21:05:54'),
(35, 25, 'qqq', 'profile-1605642178-814194.jpg', '2020-11-18 03:42:58', '2020-11-18 03:42:58'),
(37, 18, 'dsf', 'profile-1605756414-616955.png', '2020-11-19 11:26:54', '2020-11-19 11:26:54'),
(38, 18, 'ewtf', 'profile-1605825573-686363.png', '2020-11-20 06:39:33', '2020-11-20 06:39:33');

-- --------------------------------------------------------

--
-- Table structure for table `drainer_baskets`
--

CREATE TABLE `drainer_baskets` (
  `id` bigint UNSIGNED NOT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `drainer_baskets`
--

INSERT INTO `drainer_baskets` (`id`, `model`, `price`, `size`, `picture`, `created_at`, `updated_at`) VALUES
(1, 'yyt', '345', 'rre43534', 'drainer-1605581789-671911.png', '2020-11-17 10:56:29', '2020-11-17 10:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `finitions`
--

CREATE TABLE `finitions` (
  `id` bigint UNSIGNED NOT NULL,
  `matériaus_id` int NOT NULL,
  `coloris_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `finitions`
--

INSERT INTO `finitions` (`id`, `matériaus_id`, `coloris_id`, `name`, `created_at`, `updated_at`) VALUES
(20, 18, 25, 'Silk', '2020-11-08 11:36:45', '2020-11-08 11:36:45'),
(22, 8, 26, 'Adouci', '2020-11-08 11:43:16', '2020-11-08 11:43:16'),
(23, 8, 26, 'Cuir', '2020-11-08 11:43:31', '2020-11-08 11:43:31'),
(24, 8, 26, 'dfsdf', '2020-11-17 13:52:05', '2020-11-17 13:52:05'),
(25, 8, 32, 'dfsdf', '2020-11-17 13:52:47', '2020-11-17 13:52:47'),
(27, 23, 34, 'dsfsdf', '2020-11-17 21:06:06', '2020-11-17 21:06:06'),
(28, 25, 35, 'qqqq', '2020-11-18 03:43:10', '2020-11-18 03:43:10');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int NOT NULL,
  `name` char(10) DEFAULT NULL,
  `email` char(20) DEFAULT NULL,
  `phone` char(20) DEFAULT NULL,
  `address` char(20) DEFAULT NULL,
  `invoiceNum` char(10) DEFAULT NULL,
  `tva` int DEFAULT NULL,
  `percent` int DEFAULT NULL,
  `travail` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `name`, `email`, `phone`, `address`, `invoiceNum`, `tva`, `percent`, `travail`, `created_at`, `updated_at`) VALUES
(1, 'Denis', 'email', 'phone123', 'address', 'invoiceNum', 20, 42, 'travail', NULL, '2020-11-19 22:30:46');

-- --------------------------------------------------------

--
-- Table structure for table `matériaus`
--

CREATE TABLE `matériaus` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `matériaus`
--

INSERT INTO `matériaus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(8, 'Marbre', '2020-11-08 08:16:18', '2020-11-08 08:16:18'),
(18, 'Céramique Mirage', '2020-11-08 08:17:49', '2020-11-08 08:17:49'),
(23, 'dfsf', '2020-11-17 13:39:27', '2020-11-17 13:39:27'),
(24, 'black', '2020-11-17 13:54:00', '2020-11-17 13:54:00'),
(25, 'qqq', '2020-11-18 03:42:43', '2020-11-18 03:42:43'),
(26, 'sdfsdf', '2020-11-19 22:22:33', '2020-11-19 22:22:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_11_03_165532_create_users_table', 2),
(5, '2020_11_03_174740_create_matériaus_table', 3),
(6, '2020_11_04_145202_create_coloris_table', 4),
(7, '2020_11_04_145421_create_finitions_table', 4),
(8, '2020_11_04_190513_create_services_table', 5),
(9, '2020_11_04_200737_create_options_and_cuts_table', 6),
(10, '2020_11_04_203028_create_options_and_cuts_table', 7),
(11, '2020_11_04_212206_create_coloris_table', 8),
(12, '2020_11_13_185253_create_sinks_table', 9),
(13, '2020_11_13_185403_create_mixer_taps_table', 9),
(14, '2020_11_13_185452_create_soap_dispensers_table', 9),
(15, '2020_11_13_185526_create_drainer_baskets_table', 9),
(16, '2020_11_17_231214_bordures', 10);

-- --------------------------------------------------------

--
-- Table structure for table `mixer_taps`
--

CREATE TABLE `mixer_taps` (
  `id` bigint UNSIGNED NOT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mixer_taps`
--

INSERT INTO `mixer_taps` (`id`, `model`, `price`, `size`, `picture`, `created_at`, `updated_at`) VALUES
(1, 'Cerami', '195.00', 'Spout - 35 mm hole Height under spout 123 mm', 'mixer-1605367355-759394.jpg', '2020-11-14 09:59:23', '2020-11-14 10:22:35'),
(2, 'Allia', '195.00', 'Spout - Drilling 35 mm, H. 380 mm, Height under spout 258 mm', 'mixer-1605366016-613834.jpg', '2020-11-14 10:00:16', '2020-11-14 10:00:16'),
(3, 'Arrone', '195.00', 'Spout - Drilling 35 mm, Total H. 380 mm, Height under spout 302 mm', 'mixer-1605366050-756075.jpg', '2020-11-14 10:00:50', '2020-11-14 10:00:50'),
(4, 'Avisio 90', '195.00', 'Spout - Drilling 35 mm, Total H 290 mm, Height under spout 261 mm', 'mixer-1605366088-834302.jpg', '2020-11-14 10:01:28', '2020-11-14 10:01:28');

-- --------------------------------------------------------

--
-- Table structure for table `options_and_cuts`
--

CREATE TABLE `options_and_cuts` (
  `id` bigint UNSIGNED NOT NULL,
  `matériaus_id` int NOT NULL,
  `coloris_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options_and_cuts`
--

INSERT INTO `options_and_cuts` (`id`, `matériaus_id`, `coloris_id`, `name`, `picture`, `rate`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 'kjadcj', 'profile-1604522618-43215.jpg', '76', '2020-11-04 15:43:38', '2020-11-04 15:43:38'),
(2, 8, 25, 'sdfsf', 'profile-1606008056-40655.jpg', '234', '2020-11-17 13:04:16', '2020-11-22 09:20:56'),
(3, 8, 25, 'dsf', 'profile-1606008063-367560.jpg', '34', '2020-11-17 13:06:09', '2020-11-22 09:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `value1` varchar(200) DEFAULT NULL,
  `value2` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `value`, `value1`, `value2`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'banner bkg', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'banner color', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'banner text', 'banner text', NULL, NULL, '2020-11-22 16:08:02', '2020-11-22 16:08:02', NULL),
(4, 'banner logo', '', 'logo-1606032966-947389.png', NULL, '2020-11-22 16:08:03', '2020-11-22 16:16:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pagesettings`
--

CREATE TABLE `pagesettings` (
  `id` int NOT NULL,
  `title` text,
  `titlecolor` text,
  `logo` text,
  `headerimg` text,
  `footerimg` text,
  `coveragetitle` text,
  `coveragecontent` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `pagesettings`
--

INSERT INTO `pagesettings` (`id`, `title`, `titlecolor`, `logo`, `headerimg`, `footerimg`, `coveragetitle`, `coveragecontent`, `created_at`, `updated_at`) VALUES
(1, 'Denis_page', '#ff00ff', 'profile-1605801418-615802.png', 'profile-1605801418-25575.png', 'profile-1605801418-133069.png', 'coverageText-', 'coveragecontent-converage', NULL, '2020-11-19 23:56:58');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` int NOT NULL,
  `material_id` int DEFAULT NULL,
  `color_id` int DEFAULT NULL,
  `finishing_id` int DEFAULT NULL,
  `rate` int DEFAULT NULL,
  `category` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `material_id`, `color_id`, `finishing_id`, `rate`, `category`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 5, 23, 20, '2020-11-07 15:13:33', '2020-11-07 17:34:51'),
(4, 4, 1, 7, 45, NULL, '2020-11-07 15:50:39', '2020-11-07 15:50:39'),
(11, 3, 2, 5, 50, 8, '2020-11-07 17:35:30', '2020-11-07 17:35:30'),
(12, 3, 2, 5, 75, 30, '2020-11-07 17:36:01', '2020-11-07 17:36:01'),
(13, 3, 2, 5, 60, 12, '2020-11-07 17:36:14', '2020-11-07 17:36:14'),
(14, 3, 3, 3, 10, 20, '2020-11-07 19:52:28', '2020-11-07 19:52:28'),
(15, 3, 3, 4, 3, 30, '2020-11-07 19:52:40', '2020-11-07 19:52:40'),
(16, 3, 3, 3, 20, 12, '2020-11-07 19:53:23', '2020-11-07 19:53:23'),
(18, 8, 26, 22, 290, 12, '2020-11-08 19:44:10', '2020-11-08 19:44:10'),
(19, 8, 26, 22, 360, 20, '2020-11-08 19:44:35', '2020-11-08 19:44:35'),
(20, 12, 19, 14, 290, 12, '2020-11-08 19:47:31', '2020-11-08 19:47:31'),
(21, 12, 19, 14, 360, 12, '2020-11-08 19:47:55', '2020-11-08 19:47:55'),
(22, 12, 19, 14, 390, 60, '2020-11-08 19:48:17', '2020-11-08 19:48:17'),
(23, 8, 26, 22, 123, 20, '2020-11-18 03:17:13', '2020-11-18 03:17:13'),
(24, 25, 35, 28, 12312, 12, '2020-11-18 11:43:30', '2020-11-18 11:43:30'),
(25, 25, 35, 28, 2344, 20, '2020-11-18 11:43:44', '2020-11-18 11:43:59'),
(26, 8, 26, 22, 2000, 30, NULL, NULL),
(27, 25, 35, 28, 2344, 20, '2020-11-18 11:43:44', '2020-11-18 11:43:59'),
(28, 8, 26, 23, 300, 20, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `rate`, `created_at`, `updated_at`) VALUES
(1, 'Measure at home 160', '160', '2020-11-04 14:31:48', '2020-11-11 11:44:04'),
(4, 'PACKAGING + TRANSPORT + * Paris, Ile de France Region 215', '215', '2020-11-11 10:32:13', '2020-11-11 11:44:14'),
(9, 'Supplement for delivery by elevator or aids for floors, large plans, islands  270', '270', '2020-11-11 10:35:22', '2020-11-11 11:45:09'),
(10, 'Removal and removal of existing plan (s) 150', '150', '2020-11-11 10:35:39', '2020-11-11 11:45:19');

-- --------------------------------------------------------

--
-- Table structure for table `sinks`
--

CREATE TABLE `sinks` (
  `id` bigint UNSIGNED NOT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sinks`
--

INSERT INTO `sinks` (`id`, `model`, `price`, `size`, `picture`, `created_at`, `updated_at`) VALUES
(3, 'FRANKE AMX 110-16', '78.00', 'armonia - 183 x 324', 'sink-1605363598-602404.jpg', '2020-11-14 08:52:30', '2020-11-14 09:19:58'),
(4, 'BLANCO BLANCOFLEX MINI', '79', 'Blancoflex - 780 x 435', 'sink-1605364074-506585.jpg', '2020-11-14 09:27:54', '2020-11-14 09:27:54'),
(5, 'BLANCO FREE-STANDING BLANCO R-ES 8 X 6', '95', 'Blanco - 800 x 600', 'sink-1605364106-795793.jpg', '2020-11-14 09:28:26', '2020-11-14 09:28:26'),
(6, 'FRANKE GAX 110-30', '10480', 'galassia - 330 x 370', 'sink-1605364174-478666.jpg', '2020-11-14 09:29:34', '2020-11-14 09:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `soap_dispensers`
--

CREATE TABLE `soap_dispensers` (
  `id` bigint UNSIGNED NOT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `soap_dispensers`
--

INSERT INTO `soap_dispensers` (`id`, `model`, `price`, `size`, `picture`, `created_at`, `updated_at`) VALUES
(1, 'BELICE Inox Accessory pack', '76', 'BELICE - 35 mm hole, 250 ml reservoir', 'soap-1605369203-402014.jpg', '2020-11-14 10:53:23', '2020-11-14 10:53:23'),
(2, 'BELICE Stainless steel', '89', 'BELICE - 35 mm hole, 250 ml reservoir', 'soap-1605378963-101561.jpg', '2020-11-14 12:26:32', '2020-11-14 13:36:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `user` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `admin`, `user`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, 0, 'admin@admin.com', NULL, '$2a$10$EmZp4XrK7SNN.P6wkINud.YrreTR1Gvttk8g4vlGq55eEuqd3TjQW', 'QAZlvwPgN3a1tspjqGJdebmzFUhGkBy9EcrV0e8gzCoIdZjNQnzWQyC66dzq', '2020-11-03 11:58:40', '2020-11-03 11:58:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bordures`
--
ALTER TABLE `bordures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coloris`
--
ALTER TABLE `coloris`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drainer_baskets`
--
ALTER TABLE `drainer_baskets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `finitions`
--
ALTER TABLE `finitions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matériaus`
--
ALTER TABLE `matériaus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mixer_taps`
--
ALTER TABLE `mixer_taps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options_and_cuts`
--
ALTER TABLE `options_and_cuts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pagesettings`
--
ALTER TABLE `pagesettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bordures`
--
ALTER TABLE `bordures`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `coloris`
--
ALTER TABLE `coloris`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
