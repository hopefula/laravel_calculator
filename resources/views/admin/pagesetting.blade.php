@extends('layouts.admin')

@section('title')Add Matériau @endsection

@section('content')
  <div class="main-panel">
        <div class="content-wrapper">
              <div class="container-fluid">
                                  @if(Session::has('success'))
                            <div class="alert alert-success">
                                  {{Session::get('success')}}  
                            </div>
                        @endif
                        @if(Session::has('deleted'))
                            <div class="alert alert-danger">
                                  {{Session::get('deleted')}}  
                            </div>
                        @endif
                <div class="row">
                  <div class="col-lg-1">
                
                
                    </div>  

                  <div class="col-lg-10">
                  <form action='{{route('pagesetting')}}' enctype="multipart/form-data" method='post'>
                  @csrf
                  <div>
                    <h4>Title:</h4>
                    <input size='50' name='title' value={{$pagesetting[0]->title}}><br><br>
                    <h4>TitleColor:</h4>
                    <input type="color" name="titlecolor" value="#ff00ff" style="width:100px;"><br><br>
                    <h4>CoverageTitle:</h4>
                    <input size='50' name='coveragetitle' value={{$pagesetting[0]->coveragetitle}}><br><br>
                    <h4>CoverageContent:</h4>
                    <textarea name="coveragecontent" rows="4" cols="50">{{$pagesetting[0]->coveragecontent}}</textarea><br><br>
                    <h4>Select logoImage: </h4>
                    <input type="file" value="{{asset('images/stones/'.$pagesetting[0]->logo)}}" name="logo"><br><br>
                    <h4>Select Headerimage: </h4>
                    <input type="file" value="{{asset('images/stones/'.$pagesetting[0]->headerimg)}}" name="headerimg"><br><br>
                    <h4>Select logoImage: </h4>
                    <input type="file" value="{{asset('images/stones/'.$pagesetting[0]->footerimg)}}" name="footerimg">
                    <br><br>
                    <button type='submit' class='btn btn-success'>save</button> 
                  </div>
                  
                  </form>


                    </div>  


                  <div class="col-lg-1">
                
                
                    </div>  



                  </div>  


             
              </div>  
          </div>
         
        </div>
     

<!-- Modal -->

        
@endsection