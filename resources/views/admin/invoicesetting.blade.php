@extends('layouts.admin')

@section('title')Add Matériau @endsection

@section('content')
  <div class="main-panel">
        <div class="content-wrapper">
              <div class="container-fluid">
                                  @if(Session::has('success'))
                            <div class="alert alert-success">
                                  {{Session::get('success')}}  
                            </div>
                        @endif
                        @if(Session::has('deleted'))
                            <div class="alert alert-danger">
                                  {{Session::get('deleted')}}  
                            </div>
                        @endif
                <div class="row">
                  <div class="col-lg-1">
                
                
                    </div>  

                  <div class="col-lg-10">
                  @if ($message=='success') 
                  <div class="alert alert-success">
                    {{'Invoiec is set successfully'}}
                  </div>
                  @endif
                  <form action='{{route('invoicesetting')}}' enctype="multipart/form-data" method='post'>
                  @csrf
                  <div>
                    <h4>Name:</h4>
                    <input size='50' name='name' value='{{$invoices->name}}'><br><br>
                    <h4>E-Mail:</h4>
                    <input name="email" size='50' value='{{$invoices->email}}'><br><br>
                    <h4>Phone:</h4>
                    <input size='50' name='phone' value='{{$invoices->phone}}'><br><br>
                    <h4>Address:</h4>
                    <input size='50' name='address' value='{{$invoices->address}}'><br><br>
                    <h4>InvoiceNum:</h4>
                    <input size='50' name='invoiceNum' value='{{$invoices->invoiceNum}}'><br><br>
                    <h4>Tva:</h4>
                    <input type='number' size='50' name='tva' value='{{$invoices->tva}}'><br><br>
                    <h4>Percent:</h4>
                    <input type='number' size='50' name='percent' value='{{$invoices->percent}}'><br><br>
                    <h4>Travail:</h4>
                    <input size='50' name='travail' value='{{$invoices->travail}}'><br><br>
                    
                    <button type='submit' class='btn btn-success'>save</button> 
                  </div>
                  
                  </form>


                    </div>  


                  <div class="col-lg-1">
                
                
                    </div>  



                  </div>  


             
              </div>  
          </div>
         
        </div>
     

<!-- Modal -->

        
@endsection