@extends('layouts.admin')

@section('title')Add Matériau @endsection

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}  
                </div>
            @endif
            @if(Session::has('deleted'))
                <div class="alert alert-danger">
                    {{Session::get('deleted')}}  
                </div>
            @endif
            <div class="container">
                <H4>Banner</H4>
                <table class="table table-info" style="text-align: center;">
                    <thead>
                        <tr>
                            <th scope="col" style="width:5%;">#</th>
                            <th scope="col"style="width:10%;">Name</th>
                            <th scope="col"style="width:50%;">value</th>
                            <th scope="col"style="width:10%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Banner Title</td>
                            <form method="POST" action='/admin/edit-bannertitle'>
                            @csrf
                                <td>
                                    <input type="text" class="form-control" placeholder="banner title" value="{{$page['banner text']}}" name="banner_title">
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </td>
                            </form>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Banner Color</td>
                            <form method="POST" action='/admin/edit-bannercolor'>
                                @csrf
                                <td>
                                    <input type="color" name="bannercolor" value="{{$page['banner color']}}" style="width:50%;">
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </td>
                            </form>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>banner background</td>
                            <form method="POST" action='/admin/edit-background' enctype="multipart/form-data">
                                @csrf
                                <td>
                                    <div style="height:50px;">
                                        <img src="{{asset('images/stones/'.$page['banner bkg'])}}" class="img-rounded" alt="Cinque Terre">
                                    </div>
                                    <div>
                                        <input type="file" name="banner_bkg" class="col-md-4">
                                    </div>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </td>
                            </form>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td>Logo</td>
                            <form method="POST" action='/admin/edit-logo' enctype="multipart/form-data">
                                    @csrf
                                <td>
                                    <div style="height:50px;">
                                        <img src="{{asset('images/stones/'.$page['banner logo'])}}" class="img-rounded">
                                    </div>
                                    <div>
                                        <input type="file" name="logo" class="col-md-4">
                                    </div>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </td>
                            </form>
                        </tr>
                    </tbody>
                </table>

                <H4 style="margin-top: 20px;">Coverage</H4>
                <a href="/admin/add-cover" style="margin:1rem;float:right;" class="btn btn-info">Add New Coverage</a>
                <table class="table table-info">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Text</th>
                        <th scope="col">Color</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $num = 1;
                        ?>
                        @foreach($covers as $cover)
                            <tr>
                                <th scope="row">{{$num++}}</th>
                                
                                <td>{{$cover['value1']}}</td>
                                <td>
                                    <input type="color" name="titlecolor" value="{{$cover['value2']}}" style="width:50%;">
                                
                                </td>
                                <td><a type="button" href="{{route('edit-cover', $cover['id'])}}" class="btn btn-warning">Edit</a>
                                <form style="display:none;" method="POST" id="deleteCover-{{ $cover['id']}}" action="{{route('delete-cover', $cover['id'])}}">
                                    <input type="hidden" name="_token" value="">
                                </form>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteCoverModal-{{ $cover['id']}}">Delete</button></td>
                            </tr>
                        @endforeach                  
                    </tbody>
                </table>

                <H4 style="margin-top: 20px;">Latest Article</H4>
                <a href="/admin/add-article" style="margin:1rem;float:right;" class="btn btn-info">Add New Latest Article</a>
                <table class="table table-info">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Image</th>
                        <th scope="col">Title</th>
                        <th scope="col">Posted Date</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $num = 1;
                        ?>
                        @foreach($articles as $article)
                        <tr>
                            <th scope="row">{{$num++}}</th>
                            <td>
                                <div style="height:50px;">
                                    <img src="{{asset('images/stones/'.$article['value1'])}}" class="img-rounded">
                                </div>
                            </td>
                            <?php
                                $year = date('Y', strtotime($article['created_at']));
                                $Month = date('M', strtotime($article['created_at']));
                                $day = date('D', strtotime($article['created_at']));
                            ?>
                            <td>{{$article['value2']}}</td>
                            <td><?php echo "posted ".$day." ".$Month." ".$year; ?></td>
                            <td><a type="button" href="{{route('edit-article', $article['id'])}}" class="btn btn-warning">Edit</a>
                            <form style="display:none;" method="POST" id="deleteArticle-{{ $article['id']}}" action="{{route('delete-article', $article['id'])}}">
                                <input type="hidden" name="_token" value="">
                            </form>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteArticleModal-{{ $article['id']}}">Delete</button></td>
                        </tr>
                        @endforeach                
                    </tbody>
                </table>

                <H4 style="margin-top: 20px;">Popular Tag</H4>
                <a href="/admin/add-popular" style="margin:1rem;float:right;" class="btn btn-info">Add New Popular Tag</a>
                <table class="table table-info">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">tag</th>
                        <th scope="col">URL</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $num = 1;
                        ?>
                        @foreach($populars as $popular)
                        <tr>
                            <th scope="row">{{$num++}}</th>
                            <td>{{$popular['value1']}}</td>
                            <td>
                                <a href="{{$popular['value2']}}">{{$popular['value2']}}<a>
                            </td>
                            <td><a type="button" href="{{route('edit-popular', $popular['id'])}}" class="btn btn-warning">Edit</a>
                            <form style="display:none;" method="POST" id="deletePopular-{{ $popular['id']}}" action="{{route('delete-popular', $popular['id'])}}">
                                <input type="hidden" name="_token" value="">
                            </form>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletePopularModal-{{ $popular['id']}}">Delete</button></td>
                        </tr>
                        @endforeach                  
                    </tbody>
                </table>

                <H4 style="margin-top: 20px;">Social Logo</H4>
                <a href="/admin/add-social" style="margin:1rem;float:right;" class="btn btn-info">Add New Social Logo</a>
                <table class="table table-info">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Social Logo</th>
                        <th scope="col">URL</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $num = 1;
                        ?>
                        @foreach($socials as $social)
                        <tr>
                            <th scope="row">{{$num++}}</th>
                            <td>
                                <img src="{{asset('images/stones/'.$social['value1'])}}" class="img-rounded">
                            </td>
                            <td>
                                <a href="{{$social['value2']}}">{{$social['value2']}}<a>
                            </td>
                            <td>
                                <a type="button" href="{{route('edit-social', $social['id'])}}" class="btn btn-warning">Edit</a>
                                <form style="display:none;" method="POST" id="deleteSocial-{{ $social['id']}}" action="{{route('delete-social', $social['id'])}}">
                                    <input type="hidden" name="_token" value="">
                                </form>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteSocialModal-{{ $social['id']}}">Delete</button>
                            </td>
                        </tr>
                        @endforeach         
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@foreach($covers as $cover)
<div class="modal" id="deleteCoverModal-{{ $cover['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Coverage</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        You are about to delete this coverage
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <form  method="POST" id="deleteCover-{{ $cover['id'] }}" action="{{ route('delete-cover', $cover['id'])}}">@csrf
        <button type="submit" class="btn btn-primary">Yes, Delete it</button>
      </form>
      </div>
    </div>
  </div>
</div>
@endforeach

@foreach($articles as $article)
<div class="modal" id="deleteArticleModal-{{ $article['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Article</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        You are about to delete this Latest Article
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <form  method="POST" id="deleteArticle-{{ $article['id'] }}" action="{{ route('delete-article', $article['id'])}}">@csrf
        <button type="submit" class="btn btn-primary">Yes, Delete it</button>
      </form>
      </div>
    </div>
  </div>
</div>
@endforeach

@foreach($populars as $popular)
<div class="modal" id="deletePopularModal-{{ $popular['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Popular</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        You are about to delete this Latest Popular
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <form  method="POST" id="deletePopular-{{ $popular['id'] }}" action="{{ route('delete-popular', $popular['id'])}}">@csrf
        <button type="submit" class="btn btn-primary">Yes, Delete it</button>
      </form>
      </div>
    </div>
  </div>
</div>
@endforeach

@foreach($socials as $social)
<div class="modal" id="deleteSocialModal-{{ $social['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Social</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        You are about to delete this Latest Social
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <form  method="POST" id="deleteSocial-{{ $social['id'] }}" action="{{ route('delete-social', $social['id'])}}">@csrf
        <button type="submit" class="btn btn-primary">Yes, Delete it</button>
      </form>
      </div>
    </div>
  </div>
</div>
@endforeach

@endsection