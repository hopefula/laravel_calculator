@extends('layouts.admin')
@section('title')Add Mixer @endsection
@section('content')

<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-top: 5rem;">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-10" >
                    <form method="Post" action="{{route('edit-article', $article['id'])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group" >
                            <label for="model">Article Image</label>
                            <img src="{{asset('images/stones/'.$article['value2'])}}" class="img-rounded" style="width:100px;height:100px;">
                            <input type="file" class="form-control" id="fileArticle" name="fileArticle">
                        </div>
                        <div class="form-group" >
                            <label for="model">Article Text</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{$article['value1']}}" placeholder="Enter Article Text">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-lg-1">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection