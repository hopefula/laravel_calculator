@extends('layouts.admin')
@section('title')Add Mixer @endsection
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-top: 5rem;">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-10" >
                    <form method="Post" action="{{route('add-social-post')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group" >
                            <label for="model">Social Image</label>
                            <input type="file" class="form-control" id="file" name="file">
                        </div>
                        <div class="form-group" >
                            <label for="model">Social URL</label>
                            <input type="text" class="form-control" id="url" name="url" placeholder="Enter Social Text">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-lg-1">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection