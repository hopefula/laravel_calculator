@extends('layouts.admin')
@section('title')Edit Coloris @endsection
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-top: 5rem;">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-10" >
                    <form method="Post" action="{{route('edit-cover',$cover->id)}}">
                        @csrf
                        <div class="form-group" >
                            <label for="title">Coverage title</label>
                            <input type="text" required="" class="form-control" value="{{$cover->value1}}" id="title" name="title"placeholder="Enter Coverage title">
                        </div>
                        <div class="form-group" >
                            <label for="Picture">Coverage Color</label>
                            <input type="color" class="form-control" id="color" name="color" value="{{$cover->value2}}" placeholder="Enter Cover Color" style="padding:0px;">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-lg-1">
@endsection