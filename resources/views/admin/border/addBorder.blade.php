@extends('layouts.admin')
@section('title')Add Borders @endsection
@section('content')
<div class="main-panel">
   <div class="content-wrapper">
      <div class="container-fluid">
         <div class="row" style="margin-top: 5rem;">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-10" >
               @if(Session::has('success'))
               <div class="alert alert-success">
                  {{Session::get('success')}}  
               </div>
               @endif
               <form method="Post" action="{{route('adminAddBorder')}}" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group" >
                     <label for="matériau">Matériau :</label>
                     <select id="matériau" name="matériau" class="optiseldev form-control" required>
                        <option disabled selected required>Select Material</option>
                        @foreach($matériau as $matériaus)   
                        <option value="{{$matériaus->id}}">{{$matériaus->name}}</option>
                        @endforeach
                     </select>
                  </div>
                  <div class="form-group" >
                     <label for="name">Name :</label>
                     <input type="text" required="" class="form-control" id="name" name="name" aria-describedby="emailHelp" placeholder="Enter Name" required>
                  </div>
                  <div class="form-group" >
                     <label for="price">Price :</label>
                     <input type="text" required="" class="form-control" id="price" name="price" aria-describedby="emailHelp" placeholder="Enter Price" required>
                  </div>
                  <div class="form-group" >
                     <label for="Picture">Picture :</label>
                     <input type="file" required="" class="form-control" id="Picture" name="thumbnail" aria-describedby="emailHelp" placeholder="Enter Option Picture" required>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
               </form>
            </div>
            <div class="col-lg-1">
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">

</script>
@endsection