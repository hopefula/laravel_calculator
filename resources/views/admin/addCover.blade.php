@extends('layouts.admin')
@section('title')Add Mixer @endsection
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-top: 5rem;">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-10" >
                    <form method="Post" action="{{route('add-cover-post')}}">
                        @csrf
                        <div class="form-group" >
                            <label for="model">Coverage Text</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter Cover Text">
                        </div>
                        <div class="form-group" >
                            <label for="price">Coverage Color</label>
                            <input type="color" class="form-control" id="color" name="color" placeholder="Enter Cover Color" style="padding:0px;">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-lg-1">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection