@extends('layouts.admin')
@section('title')Edit Coloris @endsection
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-top: 5rem;">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-10" >
                    <form method="Post" action="{{route('edit-popular',$popular->id)}}">
                        @csrf
                        <div class="form-group" >
                            <label for="title">Popular Tag</label>
                            <input type="text" required="" class="form-control" value="{{$popular->value1}}" id="tag" name="tag"placeholder="Enter Popular title">
                        </div>
                        <div class="form-group" >
                            <label for="Picture">Popular URL</label>
                            <input type="text" class="form-control" id="url" name="url" value="{{$popular->value2}}" placeholder="Enter Popular Color">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-lg-1">
@endsection