<script>
    // fifth
    $(document).on("change", ".optiseldev.form-control", function() {
        var _this = $(this)
        var fifth_one_id = _this.val()
        var fifth_one_price = _this.attr('data-price')
        if(fifth_one_price == undefined)
            fifth_one_price = 0
        var fifth_one_name = _this.attr('data-name')
        var name = _this.attr('name')
        var CHOISISSEZ = fifth_one_price*fifth_one_id;
        
        if(CHOISISSEZ > 0){
            addOneRow('list-det-option-'+name, CHOISISSEZ, fifth_one_name)
        }else{
            var prev = $('.list-det-option-'+name);
            var prevPrice = prev.attr("data-price")
            if(prevPrice == undefined)
                prevPrice = 0
            sub_result -= parseInt(prevPrice);

            var temp_sub_result = 0;
            temp_sub_result = sub_result+215
            percentage=(20/100)*temp_sub_result;
            per_result=parseInt(percentage+temp_sub_result)

            $("#sub_total_ht").text(temp_sub_result+"€");
            $("#total_percentage").text(percentage+"€");
            $("#total_res").text(per_result+"€");
            
            prev.remove()
        }
    })

</script>