<script>
    var rate = 0
    $(document).on("change", ".sink_list", function() {
        rate = parseFloat($(this).attr('data-price'))
        console.log(rate,"check rate")
        var sink_value = $(this).val()
        console.log(sink_value*rate, sink_value, rate)
        $(this).closest("div").find('input.sink_list_value').val(sink_value*rate)
    })

    $(document).on("click", '#sink_product_value', function() {
        var sink_list = $(".sink_list_value")
        var sink_value = 0;
        console.log($(this).attr('data-price'), $(this),"rate")
        console.log(sink_list[0],"sink_list")
        sink_list.map(function(i, item) {
            if(parseFloat(item.value) > 0)
                sink_value += parseFloat(item.value)
            console.log(item.value, sink_value,"item.value")
        })
        console.log(sink_value, "sink_value item")
        
        if(sink_value > 0){
            addOneRow('list-det-sink-list', sink_value, 'Sink Product')
            // $("#totaldevis .list-det-sink-list").remove()
            // let html = `<tr class="list-det-sink-list">
            //     <td scope="row"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sink Product `+sink_value.toFixed(2)+`€ </font></font></td>
            //     <th class="prix-prod"></th>
            // </tr>`;
            // $("#totaldevis").prepend(html)
        }
        $("#Eviers").modal('hide')
    })


    $(document).on("change", ".mixer_list", function() {
        var rate = $(this).attr('data-price')
        var mixer_value = $(this).val()
        console.log(mixer_value,"mixer value",$(this).closest("div"), $(this).closest("div").find('input.mixer_list_value'))

        $(this).closest("div").find('input.mixer_list_value').val(mixer_value*rate)
    })

    $(document).on("click", '#mixer_product_value', function() {
        var mixer_list = $(".mixer_list_value")
        var mixer_value = 0;
        console.log(mixer_list[0],"mixer_list")
        mixer_list.map(function(i, item) {
            if(parseFloat(item.value) > 0)
                mixer_value += parseFloat(item.value)
        })
        console.log(mixer_value, "mixer_value item")
        
        if(mixer_value > 0){
            addOneRow('list-det-mixer-list', mixer_value, 'Mixer Product')
            // $("#totaldevis .list-det-mixer-list").remove()
            // let html = `<tr class="list-det-mixer-list">
            //     <td scope="row"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mixer Product `+mixer_value.toFixed(2)+`€ </font></font></td>
            //     <th class="prix-prod"></th>
            // </tr>`;
            // $("#totaldevis").prepend(html)
        }
        $("#Mitigeurs").modal('hide')
    })


    $(document).on("change", ".soap_list", function() {
        var rate = $(this).attr('data-price')
        var soap_value = $(this).val()
        console.log(soap_value,"soap value",$(this).closest("div"), $(this).closest("div").find('input.soap_list_value'))

        $(this).closest("div").find('input.soap_list_value').val(soap_value*rate)
    })

    $(document).on("click", '#soap_product_value', function() {
        var soap_list = $(".soap_list_value")
        var soap_value = 0;
        console.log(soap_list[0],"soap_list")
        soap_list.map(function(i, item) {
            if(parseFloat(item.value) > 0)
                soap_value += parseFloat(item.value)
        })
        console.log(soap_value, "soap_value item")
        
        if(soap_value > 0){
            addOneRow('list-det-soap-list', soap_value, 'Soap Product')
            // $("#totaldevis .list-det-soap-list").remove()
            // let html = `<tr class="list-det-soap-list">
            //     <td scope="row"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Soap Product `+soap_value.toFixed(2)+`€ </font></font></td>
            //     <th class="prix-prod"></th>
            // </tr>`;
            // $("#totaldevis").prepend(html)
        }
        $("#Distributeur-Savon").modal('hide')
    })
</script>    