<script>    
    var reduced_vat = 0;
    var vat_percentage = "";

    $(document).on('click', '.reduced_vat', function(){
        let html = "";
        var tot_per = 0;

        if($(this).is(":checked")){
            sub_per = 10
            vat_percentage = "REDUCED VAT 10%"
        }
        else if($(this).is(":not(:checked)")){
            sub_per = 20
            vat_percentage = "20% VAT"
        }

        var temp_sub_result = 0;
        temp_sub_result = sub_result+215
        percentage=(sub_per/100)*temp_sub_result;
        per_result=parseFloat(percentage+temp_sub_result)
        
        $("#sub_total_ht").text(temp_sub_result.toFixed(2)+"€");
        $("#total_percentage").text(percentage.toFixed(2)+"€");
        $("#total_res").text(per_result.toFixed(2)+"€");
        console.log(vat_percentage, "vat_percentage", sub_per)
        document.getElementById("vat_percentage").innerHTML = vat_percentage;
    })
</script>    
