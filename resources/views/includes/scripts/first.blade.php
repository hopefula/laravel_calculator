<script>
   //Change base url
      var base_url='';
      var sub_result=0;
      var sub_per = 20
      var percentage=0;
      var per_result=0;
      var totaldevis = $("#totaldevis")
      var sub_total= 0;
      
   
   $(document).ready(function () {
        
        $('#edit-worktop-thickness').one('change',function(e) {					
            $("#totaldevis").prepend('<tr id="table_'+i+'" class="list-det-devis" data-name="+ PACKAGING + TRANSPORT + * Paris, Ile de France region" data-price="215"><td scope="row"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+ PACKAGING + TRANSPORT + * Paris, Ile de France region</font></font><h5>215.00€</h5></td><th class="prix-prod"></th></tr>');
        })
     
        var i=0;
        $('#edit-worktop-thickness').on('change',function(e) {
            var total_amount= '';

            var editWorktopThickness=$(this).val();
            var category=$(this).val();

            var length=$("#edit-worktop-length").val();
            var width=$("#edit-worktop-width").val();
            var thickness=$("#edit-worktop-width").val();

            var calculation = (length/1000) * (width/1000);

            var res=calculation.toFixed(6)

            var material_id = $('#matériau').val();
            var color_id = $('#coloris').val();
            var finishing_id = $('#finition').val();


            $.ajax({
                
                url:"{{ route('get-calculated-price') }}",
                type:"GET",
                data: {  
                    material_id: material_id,
                    color_id: color_id,
                    finishing_id: finishing_id,
                    category: category,
                },
                
                success:function (data) {
                    i++;
                    console.log(data)
                    if(data['rates'] != null)
                    {
                        total_amount = (parseFloat(res * data['rates']['rate'])).toFixed(6);
                        total_amount = parseFloat(total_amount); 
                        sub_result += total_amount;
                    
                        var temp_sub_result = 0;
                        temp_sub_result = sub_result+215
                        percentage=(sub_per/100)*temp_sub_result;
                        per_result=parseFloat(percentage+temp_sub_result)
                        
                        $("#sub_total_ht").text(temp_sub_result.toFixed(2)+"€");
                        $("#total_percentage").text(percentage.toFixed(2)+"€");
                        $("#total_res").text(per_result.toFixed(2)+"€");
                        if(total_amount > 0) {
                            console.log(typeof total_amount, "total_amount")
                            console.log(total_amount.toFixed(2))
                            $("#calculation").append('<div id="row_'+i+'_1" class="col-6 table-responsive" data-name="Worktop '+length+'mm x '+width+'mm thickness '+category+' mm" data-price="'+per_result+'"><table id="crtable" class="sticky-enabled total-dim table"><tbody><tr class="d-none"><td width="5%"></td><td width="17%"></td><td width="3%"></td><td width="17%"></td><td width="3%"></td><td width="17%"></td><td width="3%"></td><td width="18%"></td><td width="17%"></td></tr><tr id="trcredence0"><td width="5%">  </td><td width="17%"><span>'+length+'mm</span></td><td width="3%">x</td><td width="17%"><span>'+width+'mm</span></td><td width="3%">x</td><td width="17%"><span>'+category+'mm</span></td><td width="3%">=</td><td width="18%">'+total_amount+'€</td><td width="17%" class="total-dim-sup"><a href="javascript:void(0)" class="delcredence" onclick="removeRow('+i+','+total_amount+', 1)" name="worktop_delete-0">X</a></td></tr></tbody></table></div>');
                            $("#totaldevis").prepend('<tr id="table_'+i+'_1" class="list-det-devis" data-name="Worktop '+length+'mm x '+width+'mm thickness '+category+' mm" data-price="'+per_result+'"><td scope="row"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Worktop '+length+'mm x '+width+'mm thickness '+category+' mm</font></font><h5>'+total_amount.toFixed(2)+'€</h5></td><th class="prix-prod"></th></tr>');
                        }
                    }
                }
            })
        })
   
        $('#coloris').on('change',function(e) {

            var color_id = $('#coloris').val();
            var mat_id = $('#matériau').val();
            $.ajax({
                
                url:"{{ route('get-finishing') }}",
                type:"POST",
                data: {
                    "_token": "{{ csrf_token() }}",   
                color_id: color_id,
                mat_id: mat_id,
                },
                
                success:function (data) {
                    var my_url=base_url+'/images/stones/';
                    if(data['color_img'] == "")
                        data['color_img'] == "profile-1604529971-269365.jpg";
                    $('#material_img').attr('src',my_url+data['color_img']);

                    $('#finition').empty();
                    if(data['finishing'].length > 0) {
                        
                        $('#finition').append('<<option selected disabled required>Select Finition</option>');

                        $.each(data['finishing'],function(index,item){
                            $('#finition').append('<option value="'+item.id+'">'+item.name+'</option>');
                        })
                    }

                    var material_text = $('#matériau option:selected').text();

                    var color_text = $('#coloris option:selected').text();

                    $("#main_heading").text(material_text+" "+color_text);
                }
            })
        });

        $(document).on('change', '#finition', function(e){
                var material_id = $('#matériau').val();
                var color_id = $('#coloris').val();
                var finishing_id = $('#finition').val();

                    $.ajax({
                
                        url:"{{ route('get-price') }}",
                        type:"GET",
                        data: {  
                            material_id: material_id,
                            color_id: color_id,
                            finishing_id: finishing_id
                        },
                        
                        success:function (data) {
                            console.log(data, "check data in end")
                            $("#show_rates").empty();
                            for(var i=0 ; i<data['rates'].length ; i++)
                            {
                                $("#show_rates").append('<span>'+data['rates'][i]['category']+" mm "+ data['rates'][i]['rate']+' € HT</span><br/>');
                            }
                            var borders = data['borders'];

                            $("#edit-worktop-edge").empty()
                            $('#edit-worktop-edge').append('<option disabled selected required>Select Bordure</option>');
                            $.each(borders, function(index,item){
                                $('#edit-worktop-edge').append('<option value="'+item.id+'" data-name="'+item.name+'" data-picture="'+item.picture+'" data-price="'+item.price+'">'+item.name+'</option>');
                            })
                    }
            })

        });
   });
   
    function removeRow(row,amount,order)
    {
        sub_result -= parseFloat(amount);

        var temp_sub_result = 0;
        temp_sub_result = sub_result+215
        percentage=(sub_per/100)*temp_sub_result;
        per_result=parseFloat(percentage+temp_sub_result)
        
        $("#sub_total_ht").text(temp_sub_result+"€");
        $("#total_percentage").text(percentage+"€");
        $("#total_res").text(per_result+"€");

        $("#row_"+row+"_"+order).html('');
        $("#table_"+row+"_"+order).html('');
    }
   
    function addOneRow(row, amount, text)
    {   
        if(isNaN(sub_result))
            sub_result = 0;

        var old_price = 0;
        if($("."+row).length > 0)
            old_price = $("."+row).attr("data-price");
            
        sub_result -= parseFloat(old_price);
        sub_result += parseFloat(amount);
        
        var temp_sub_result,per_result,percentage  = 0;
        temp_sub_result = sub_result+215
        percentage=(sub_per/100)*temp_sub_result;
        per_result=parseFloat(percentage+temp_sub_result)
        
        $("#sub_total_ht").text(temp_sub_result.toFixed(2)+"€");
        $("#total_percentage").text(percentage.toFixed(2)+"€");
        $("#total_res").text(per_result.toFixed(2)+"€");

        $("#totaldevis ."+row).remove()
        let html = `<tr class="`+row+`" data-name="`+text+`" data-price="`+amount+`">
            <td scope="row"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">`+text+" <b>"+amount+`€ </b></font></font></td>
            <th class="prix-prod"></th>
        </tr>`;
        $("#totaldevis").prepend(html)
        
    }
    
    // function removeRow(row,amount,name)
    // {
    //     var result=(sub_result-amount);
    //     sub_result=parseInt(result);
    //     var per=(20/100)*amount;
    //     var tot_per=percentage-per;
    //     percentage=tot_per;
    //     var final_per=(sub_result+percentage);
    //     per_result=final_per;
    //     $("#total_res").text(per_result+"€");
    //     $("#total_percentage").text(tot_per+"€");
    //     $("#sub_total_ht").text(result+"€");
    //     $("#row_"+name).html('');
    // }
    $(document).on('change', '#edit-worktop-edge', function() {
        var option_name  = $('option:selected', this).attr('data-name');
        var option_price = $('option:selected', this).attr('data-price');
        var option_image = $('option:selected', this).attr('data-picture');
        $('#border_image').html('<img class="col-lg-4" src="'+option_image+'" style="width:100%;max-height:100px;">')
        if(parseFloat(option_price) > 0)
            addOneRow('list-det-worktop-edge', option_price, option_name)
    })
</script>