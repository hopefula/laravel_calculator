<script>
    var i = 0;
    // fourth
    $(document).on("change", "#edit-worktop-thickness2", function() {
    // $("#edit-worktop-thickness2").change(function() {
        var total_amount = '';

        var editWorktopThickness = $(this).val();
        var category = $(this).val();

        var length = $("#edit-worktop-CREDENCE-length").val();
        var width = $("#edit-worktop-CREDENCE-width").val();
        var thickness = $("#edit-worktop-CREDENCE-width").val();

        var calculation = (length / 1000) * (width / 1000);

        var res = calculation.toFixed(6)

        var material_id = $('#matériau').val();
        var color_id = $('#coloris').val();
        var finishing_id = $('#finition').val();

        $.ajax({

            url: "{{ route('get-calculated-price') }}",
            type: "GET",
            data: {
                material_id: material_id,
                color_id: color_id,
                finishing_id: finishing_id,
                category: category,
            },

            success: function (data) {
                i++;
                console.log(data)
                if (data['rates'] != null) {

                    // total_amount = parseInt(res * data['rates']['rate']);
                    // total_amount = total_amount.toFixed(2)
                    // sub_total += total_amount;
                
                    // sub_result+= parseInt(sub_total)
                    // // sub_result = sub_result.toFixed(2)

                    // percentage=(20/100)*sub_result;
                
                    // per_result=parseInt(percentage+sub_result)
                    // per_result = per_result.toFixed(2);

                    // $("#total_res").text(per_result+"€");

                    // $("#total_percentage").text(percentage.toFixed(2)+"€");

                    // $("#sub_total_ht").text(sub_result + "€");

                    total_amount = (parseFloat(res * data['rates']['rate']));
                    sub_result += parseFloat(total_amount);
                
                    var temp_sub_result = 0;
                    temp_sub_result = sub_result+215
                    percentage=(20/100)*temp_sub_result;
                    per_result=parseFloat(percentage+temp_sub_result)
                    
                    $("#sub_total_ht").text(temp_sub_result.toFixed(2)+"€");
                    $("#total_percentage").text(percentage.toFixed(2)+"€");
                    $("#total_res").text(per_result.toFixed(2)+"€");
                    
                    if(total_amount > 0) {
                        $("#calculation2").append('<div id="row_' + i + '_2" data-name="'+"SIZE OF YOUR CREDENCE"+'" data-price="'+total_amount.toFixed(2)+'" class="col-6 table-responsive"><table id="crtable" class="sticky-enabled total-dim table"><tbody><tr class="d-none"><td width="5%"></td><td width="17%"></td><td width="3%"></td><td width="17%"></td><td width="3%"></td><td width="17%"></td><td width="3%"></td><td width="18%"></td><td width="17%"></td></tr><tr id="trcredence0"><td width="5%">  </td><td width="17%"><span>' + length + 'mm</span></td><td width="3%">x</td><td width="17%"><span>' + width + 'mm</span></td><td width="3%">x</td><td width="17%"><span>' + category + 'mm</span></td><td width="3%">=</td><td width="18%">' + total_amount + '€</td><td width="17%" class="total-dim-sup"><a href="javascript:void(0)" class="delcredence" onclick="removeRow(' + i + ',' + total_amount + ', 2)" name="worktop_delete-0">X</a></td></tr></tbody></table></div>');
                        $("#totaldevis").prepend('<tr id="table_' + i + '_2" class="list-det-devis" data-name="Worktop '+length+'mm x '+width+'mm thickness '+category+' mm" data-price="'+per_result+'"><td scope="row"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Worktop ' + length + 'mm x ' + width + 'mm thickness ' + category + ' mm</font></font><h5>' + total_amount.toFixed(2) + '€</h5></td><th class="prix-prod"></th></tr>');
                    }

                }

            }
        })
    })
</script>