@extends('layouts.master')
@section('title')Services @endsection
@section('header')
<section class="dark-wrapper opaqued parallax" data-parallax="scroll" style="background:url({{asset('images/stones/'.$pagesetting[0]->headerimg)}}); background-repeat: no-repeat;background-size: cover;" data-speed="0.7">
   <div class="section-inner pad-top-200">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 mt30 wow text-center">
               <h2 class="section-heading" style="color: {{ array_key_exists('banner color', $page) ? $page['banner color'] : ''}}">{{ array_key_exists('banner text', $page) ? $page['banner text'] : ""}}</h2>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section('content')
<div class="container-fluid">
   <div class="col-lg-1">
   </div>
   <div class="col-lg-10">
      <div class="row" id="material_description">
         <div class="col-lg-4">
            <img id="material_img" src="{{asset('Kompleet/assets/img/bg/bg2.jpg')}}" width="100%" height="30%" style="margin-top: 2rem;">							
         </div>
         <div class="col-lg-8">
            <h4 id="main_heading" style="margin-top: 2rem; color: #fff; background-color: black; padding: 1.5rem;"></h4>
            <p>Estimate the price of your kitchen or bathroom project directly, thanks to our simulation tool.
               Fill in the following fields. Validate. The software calculates the cost of your work plan in real time.
            </p>
         </div>
      </div>
      <h1 class="title-header hidden customer_area">RECEVOIR VOTRE DEVIS PAR EMAIL</h1>
      <div class="row" style="margin-top: 4rem;">
         <div class="col-lg-7 calculator_area">
            <div class="row">
               <div class="col-lg-12">
                  <h6>1 - VOTRE MATÉRIAUX </h6>
                  <div class="container-fluid">
                     <div class="col-lg-10">
                        <div class="form-group row">
                           <div class="col-lg-5">
                              <label for="matériau">Matériau :</label>
                           </div>
                           <div class="col-lg-5">
                              <div class="input-group" >
                                 <input type = "hidden" name = "_token" value = '<?php echo csrf_token(); ?>'>
                                 <select class="optiseldev" name="matériau" id="matériau" onchange="this.options[this.selectedIndex].value && (window.location = '/selectMaterial/'+this.options[this.selectedIndex].value);">
                                    <option disabled selected required>Select Material</option>
                                    @foreach ($matériau as $item)
                                    <option value="{{ $item->id }}" {{ ( isset($material) && ($item->id == $material->id)) ? 'selected':'' }}>{{ $item->name }}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group" >
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="container-fluid">
                     <div class="col-lg-10">
                        <div class="form-group row">
                           <div class="col-lg-5">
                              <label for="coloris">Coloris :</label>
                           </div>
                           <div class="col-lg-5">
                              <div class="input-group" >
                                 <select class="browser-default custom-select" name="coloris" id="coloris">
                                    <option selected disabled required>Select Colors</option>
                                    @if(isset($colors))
                                    @foreach ($colors as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                    @endif
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group" >
                              </div>
                           </div>
                        </div>
                        <div class="form-group row">
                           <div class="col-lg-5">
                              <label for="finition">Finition :</label>
                           </div>
                           <div class="col-lg-5">
                              <div class="input-group" >
                                 <select class="browser-default custom-select" name="finition" id="finition">
                                    <option disabled selected required>Select Finishing</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-2">
                              <div class="input-group" >
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="show_rates">
            </div>
            <p>Ces prix sont indiqués par mètre carré de plan.</p>
            <div class="row" style="margin-top: 4rem;">
               <div class="col-lg-12">
                  <h6>2 - Taille de votre plan </h6>
                  <div class="container-fluid">
                     <div class="col-lg-10">
                        <div class="row hidden-print mb-30">
                           <div class="col-sm-4 col-12 taille-plan">
                              <div class="form-group row">
                                 <div class="col-sm-12 col-5"><label>Longueur (mm) :</label></div>
                                 <div class="col-sm-12 col-7">
                                    <input class="form-control" id="edit-worktop-length" name="worktop[length]" maxlength="128">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4 col-12 taille-plan">
                              <div class="form-group row">
                                 <div class="col-sm-12 col-5"><label>Largeur (mm) :</label></div>
                                 <div class="col-sm-12 col-7">
                                    <input class="form-control" id="edit-worktop-width" name="worktop[width]">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4 col-12 taille-plan">
                              <div class="form-group row">
                                 <div class="col-sm-12 col-5"><label>Epaisseur (mm) :</label></div>
                                 <div class="col-sm-12 col-7">
                                    <select class="form-control" id="edit-worktop-thickness" name="worktop[thickness]">
                                       <option value="0"> - Sélectionner - </option>
                                       <option value="12">12 mm</option>
                                       <option value="20">20 mm</option>
                                       <option value="30">30 mm</option>
                                       <option value="60">60 mm</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12">
                  <div id="calculation">
                  </div>
               </div>
            </div>
            <div class="row" style="margin-top: 4rem;">
               <div class="col-lg-12">
                  <h6>3 - VOTRE FORME DE BORDURE </h6>
                  <div class="container-fluid">
                     <div class="col-lg-8">
                        <label>chant du plan de travail: (forme de la bordure du plan)</label>
                        <select id="edit-worktop-edge" class="form-control" name="worktop_edge">
                           <option disabled selected required>Select Bordure</option>
                           @if(isset($borders))
                           @foreach ($borders as $item)
                           <option value="{{ $item->id }}" data-name="{{ $item->name }}" data-price="{{ $item->price }}" data-picture="{{ asset('images/border/'.$item->picture) }}">{{ $item->name }}</option>
                           @endforeach
                           @endif
                        </select>
                     </div>
                     <div id="border_image" class="col-lg-4">
                     </div>
                  </div>
               </div>
            </div>
            <div class="row" style="margin-top: 4rem;">
               <div class="col-lg-12">
                  <h6>4 - SIZE OF YOUR CREDENCE
                  </h6>
                  <div class="container-fluid">
                     <div class="col-lg-10">
                        <div class="row hidden-print mb-30">
                           <div class="col-sm-4 col-12 taille-plan">
                              <div class="form-group row">
                                 <div class="col-sm-12 col-5"><label>Longueur (mm) :</label></div>
                                 <div class="col-sm-12 col-7">
                                    <input class="form-control" id="edit-worktop-CREDENCE-length" name="worktop[length]" maxlength="128">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4 col-12 taille-plan">
                              <div class="form-group row">
                                 <div class="col-sm-12 col-5"><label>Largeur (mm) :</label></div>
                                 <div class="col-sm-12 col-7">
                                    <input class="form-control" id="edit-worktop-CREDENCE-width" name="worktop[width]">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4 col-12 taille-plan">
                              <div class="form-group row">
                                 <div class="col-sm-12 col-5"><label>Epaisseur (mm) :</label></div>
                                 <div class="col-sm-12 col-7">
                                    <select class="form-control" id="edit-worktop-thickness2" name="worktop[thickness]">
                                       <option value="0"> - Sélectionner - </option>
                                       <option value="20">20 mm</option>
                                       <option value="30">30 mm</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			<div class="row calculator_area">
               <div class="col-lg-12">
                  <div id="calculation2">
                  </div>
               </div>
            </div>
            <div class="row" style="margin-top: 4rem;">
               <div class="col-lg-10">
                  <h6>5 - CHOISISSEZ LES OPTIONS ET DÉCOUPES</h6>
                  <div class="container-fluid">
                     <div class="col-lg-12">
                        <div class="row hidden-print mb-30">
                           <div class="row decoup" style="margin-top: 1rem;">

                              @if(count($optionsAndCuts) > 0)
                              @foreach($optionsAndCuts as $temp)
                                 <div class="col-md-3 col-sm-4 col-6 opt_decoup mb-30 text-center">
                                    <div class="opt_img">
                                       <div style="height:65px; margin:0; padding:0"><img src="{{asset('images/'.$temp->picture)}}" height="100%" width="100%" alt="{{$temp->name}}" ></div>
                                    </div>
                                    <p>{{$temp->name}}</p>
                                    <div class="prix-opt-decoup">
                                       {{$temp->rate}} € <span>HT</span> <small class="preview_price" id="optot{{$temp->id}}"></small>
                                    </div>
                                    <select id="edit-options-{{$temp->id}}-qty" name="{{$temp->id}}" data-name="{{$temp->name}}" data-price = "{{$temp->rate}}" class="optiseldev form-control">
                                       <option value="0">0</option>
                                       <option value="1">1</option>
                                       <option value="2">2</option>
                                       <option value="3">3</option>
                                       <option value="4">4</option>
                                       <option value="5">5</option>
                                    </select>
                                 </div>
                              @endforeach
                              @endif

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row" style="margin-top: 4rem;">
               <div class="col-lg-12">
                  <h6>6 - OUR ACCESSORIES</h6>
                  <div class="container-fluid">
                     <div class="col-lg-10">
                        <div class="row hidden-print mb-30">
                           <div class="row decoup devaccessoire">
                              
                              <div class="col-md-3 col-sm-4 col-6 opt_decoup mb-30 text-center">
                                 <div class="opt_img">
                                    <a href="#" data-toggle="modal" data-target="#Eviers" class="accdev_modal hidden-print">
                                       <img src="{{asset('images/accessories/acc_eviers.jpg')}}" height="15%" width="100%" alt="Eviers">
                                    </a>
                                 </div>
                                 <p><button type="button" class="btn" style="background-color: #fff;" data-toggle="modal" data-target="#Eviers">Eviers </button></p>
                              </div>
                              
                              <div class="col-md-3 col-sm-4 col-6 opt_decoup mb-30">
                                 <div class="opt_img">
                                    <a href="#" data-toggle="modal" data-target="#Mitigeurs" class="accdev_modal hidden-print">
                                    <img src="{{asset('images/accessories/acc_mitigeurs.jpg')}}" height="15%" width="100%" alt="Mitigeurs">
                                    </a>
                                 </div>
                                 <p><button type="button" class="btn" style="background-color: #fff;" data-toggle="modal" data-target="#Mitigeurs">Mitigeurs </button></p>
                              </div>
                              <div class="col-md-3 col-sm-4 col-6 opt_decoup mb-30">
                                 <div class="opt_img">
                                    <a href="#" data-toggle="modal" data-target="#Distributeur-Savon" class="accdev_modal hidden-print">
                                    <img src="{{asset('images/accessories/acc_distributeur-savon.jpg')}}" height="15%" width="100%" alt="Distributeur Savon">
                                    </a>
                                 </div>
                                 <p><button type="button" class="btn" style="background-color: #fff;" data-toggle="modal" data-target="#Distributeur-Savon">Distributeur Savon</button></p>
                              </div>
                              <div class="col-md-3 col-sm-4 col-6 opt_decoup mb-30 hidden d-none">
                                 <div class="opt_img">
                                    <a href="#" data-toggle="modal" data-target="#Panier-égouttoir" class="accdev_modal hidden-print">
                                    <img src="{{asset('images/accessories/acc_panier_egouttoire.jpg')}}" height="15%" width="100%" alt="Panier égouttoir">
                                    </a>
                                 </div>
                                 <p><button type="button" class="btn" style="background-color: #fff;" data-toggle="modal" data-target="#Panier-égouttoir">Panier égouttoir</button></p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row" style="margin-top: 4rem;">
               <div class="col-lg-12">
                  <h6>7 - OUR SERVICES</h6>
                  <div class="container-fluid">
                     <div class="col-lg-10">
                        @foreach($services as $service)
                        <div class="form-check">
                           <input class="form-check-input service_checkbox" type="checkbox" name="{{$service->name}}" id="{{$service->name}}" value="{{$service->rate}}" >
                           <label style="margin-left: 0.5rem;" class="form-check-label" for="{{$service->name}}">
                              <p style="font: 17px serif;"> {{$service->name}}  € HT</p>
                           </label>
                        </div>
                        @endforeach
                     </div>
                  </div>
               </div>
            </div>
            <div class="row" style="margin-top: 4rem;">
               <div class="col-lg-12">
                  <h6>8 - REDUCED VAT</h6>
                  <div class="container-fluid">
                     <div class="col-lg-10">
                        <div class="form-check">
                           <label style="margin-left: 0.5rem;" class="form-check-label" for="reduced_vat">
                              <input class="form-check-input reduced_vat" type="checkbox" name="REDUCED VAT" id="reduced_vat" value="REDUCED VAT" >
                              <span style="font: 17px serif;"> 
                                 Residence for more than 2 years 10% VAT
                                 10% VAT applicable to housing for residential use over 2 years if the installation is carried out by us, if not 20% VAT
                              </span>
                           </label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row" style="margin-top: 4rem;">
               <div class="col-lg-12">
                  <h6>9 - EASY PLAN STORES </h6>
                  <div class="container-fluid">
                     <div class="col-8">
                        <p>Which store is closest to you?</p>
                        <div class="row">
                           <div class="col-lg-4">
                              <input class="plan_store_radio" type="radio" id="Paris02" name="store" value="1">
                              <label for="Paris02">
                                 <p style="font: 17px serif;">Paris 02</p>
                              </label>
                              <br>
                           </div>
                           <div class="col-lg-4">
                              <input class="plan_store_radio" type="radio" id="Paris08" name="store" value="2">
                              <label for="Paris08">
                                 <p style="font: 17px serif;">Paris 08</p>
                              </label>
                              <br> 
                           </div>
                           <div class="col-lg-4">
                              <input class="plan_store_radio" type="radio" id="Garches92" name="store" value="3">
                              <label for="age3">
                                 <p style="font: 17px serif;">Garches 92</p>
                              </label>
                              <br><br>
                           </div>
                           <div class="col-lg-4">
                              <input class="plan_store_radio" type="radio" id="Rouen76" name="store" value="4">
                              <label for="Rouen76">
                                 <p style="font: 17px serif;">Rouen 76</p>
                              </label>
                              <br><br>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-7 hidden customer_area">
         <div class="col-md-11 col-12 mb-30" id="submitform">
            <h4 class="center_border smtitle"><span>Contact</span></h4>
            <p class="para-header">Merci de remplir le formulaire ci-dessous pour recevoir votre devis détaillé par email :</p>
            <input type="hidden" name="email" class="email" />
            <form>
               <table class="col-md-12">
                  <tbody>
                     <tr>
                        <td><input type="text" name="votre_nom" class="form-control text-control" placeholder="Nom*" required=""></td>
                        <td>
                           <input type="email" name="votre_email" class="form-control mail-control" placeholder="Email*" required="">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <input type="text" name="votre_tel" class="form-control text-control" placeholder="Numero de télephone*" required="">
                        </td>
                        <td>
                           <select class="form-control" id="votre-pays" name="votre-pays" required>
                              <option value="AF">Afghanistan</option>
                              <option value="ZA">Afrique du Sud</option>
                              <option value="AL">Albanie</option>
                              <option value="DZ">Algérie</option>
                              <option value="DE">Allemagne</option>
                              <option value="AD">Andorre</option>
                              <option value="AO">Angola</option>
                              <option value="AI">Anguilla</option>
                              <option value="AQ">Antarctique</option>
                              <option value="AG">Antigua-et-Barbuda</option>
                              <option value="AN">Antilles néerlandaises</option>
                              <option value="SA">Arabie Saoudite</option>
                              <option value="AR">Argentine</option>
                              <option value="AM">Arménie</option>
                              <option value="AW">Aruba</option>
                              <option value="AU">Australie</option>
                              <option value="AT">Autriche</option>
                              <option value="AZ">Azerbaïdjan</option>
                              <option value="BS">Bahamas</option>
                              <option value="BH">Bahreïn</option>
                              <option value="BD">Bangladesh</option>
                              <option value="BB">Barbade</option>
                              <option value="BE">Belgique</option>
                              <option value="BZ">Belize</option>
                              <option value="BM">Bermudes</option>
                              <option value="BT">Bhoutan</option>
                              <option value="MM">Birmanie</option>
                              <option value="BY">Biélorussie</option>
                              <option value="BO">Bolivie</option>
                              <option value="BA">Bosnie-Herzégovine</option>
                              <option value="BW">Botswana</option>
                              <option value="BN">Brunei</option>
                              <option value="BR">Brésil</option>
                              <option value="BG">Bulgarie</option>
                              <option value="BF">Burkina Faso</option>
                              <option value="BI">Burundi</option>
                              <option value="BJ">Bénin</option>
                              <option value="KH">Cambodge</option>
                              <option value="CM">Cameroun</option>
                              <option value="CA">Canada</option>
                              <option value="CV">Cap-Vert</option>
                              <option value="BQ">Caribbean Netherlands</option>
                              <option value="CL">Chili</option>
                              <option value="CN">Chine</option>
                              <option value="CY">Chypre</option>
                              <option value="CO">Colombie</option>
                              <option value="KM">Comores</option>
                              <option value="CG">Congo (Brazzaville)</option>
                              <option value="CD">Congo (Kinshasa)</option>
                              <option value="KP">Corée du Nord</option>
                              <option value="KR">Corée du Sud</option>
                              <option value="CR">Costa Rica</option>
                              <option value="HR">Croatie</option>
                              <option value="CU">Cuba</option>
                              <option value="CW">Curaçao</option>
                              <option value="CI">Côte d’Ivoire</option>
                              <option value="DK">Danemark</option>
                              <option value="DJ">Djibouti</option>
                              <option value="DM">Dominique</option>
                              <option value="ES">Espagne</option>
                              <option value="EE">Estonie</option>
                              <option value="FJ">Fidji</option>
                              <option value="FI">Finlande</option>
                              <option value="FR" selected="selected">France</option>
                              <option value="GA">Gabon</option>
                              <option value="GM">Gambie</option>
                              <option value="GH">Ghana</option>
                              <option value="GI">Gibraltar</option>
                              <option value="GD">Grenade</option>
                              <option value="GL">Groenland</option>
                              <option value="GR">Grèce</option>
                              <option value="GP">Guadeloupe</option>
                              <option value="GU">Guam</option>
                              <option value="GT">Guatemala</option>
                              <option value="GG">Guernesey</option>
                              <option value="GN">Guinée</option>
                              <option value="GW">Guinée-Bissau</option>
                              <option value="GQ">Guinée équatoriale</option>
                              <option value="GY">Guyana</option>
                              <option value="GF">Guyane française</option>
                              <option value="GE">Géorgie</option>
                              <option value="GS">Géorgie du Sud et les Îles Sandwich du Sud</option>
                              <option value="HT">Haïti</option>
                              <option value="HN">Honduras</option>
                              <option value="HK">Hong Kong R.A.S., Chine</option>
                              <option value="HU">Hongrie</option>
                              <option value="IN">Inde</option>
                              <option value="ID">Indonésie</option>
                              <option value="IQ">Irak</option>
                              <option value="IR">Iran</option>
                              <option value="IE">Irlande</option>
                              <option value="IS">Islande</option>
                              <option value="IL">Israël</option>
                              <option value="IT">Italie</option>
                              <option value="JM">Jamaïque</option>
                              <option value="JP">Japon</option>
                              <option value="JE">Jersey</option>
                              <option value="JO">Jordanie</option>
                              <option value="KZ">Kazakhstan</option>
                              <option value="KE">Kenya</option>
                              <option value="KG">Kirghizistan</option>
                              <option value="KI">Kiribati</option>
                              <option value="KW">Koweït</option>
                              <option value="LA">Laos</option>
                              <option value="LS">Lesotho</option>
                              <option value="LV">Lettonie</option>
                              <option value="LB">Liban</option>
                              <option value="LR">Libéria</option>
                              <option value="LI">Liechtenstein</option>
                              <option value="LT">Lituanie</option>
                              <option value="LU">Luxembourg</option>
                              <option value="LY">Lybie</option>
                              <option value="MO">Macao R.A.S., Chine</option>
                              <option value="MK">Macédoine</option>
                              <option value="MG">Madagascar</option>
                              <option value="MY">Malaisie</option>
                              <option value="MW">Malawi</option>
                              <option value="MV">Maldives</option>
                              <option value="ML">Mali</option>
                              <option value="MT">Malte</option>
                              <option value="MA">Maroc</option>
                              <option value="MQ">Martinique</option>
                              <option value="MU">Maurice</option>
                              <option value="MR">Mauritanie</option>
                              <option value="YT">Mayotte</option>
                              <option value="MX">Mexique</option>
                              <option value="FM">Micronésie</option>
                              <option value="MD">Moldavie</option>
                              <option value="MC">Monaco</option>
                              <option value="MN">Mongolie</option>
                              <option value="MS">Montserrat</option>
                              <option value="ME">Monténégro</option>
                              <option value="MZ">Mozambique</option>
                              <option value="NA">Namibie</option>
                              <option value="NR">Nauru</option>
                              <option value="NI">Nicaragua</option>
                              <option value="NE">Niger</option>
                              <option value="NG">Nigeria</option>
                              <option value="NU">Niue</option>
                              <option value="NO">Norvège</option>
                              <option value="NC">Nouvelle-Calédonie</option>
                              <option value="NZ">Nouvelle-Zélande</option>
                              <option value="NP">Népal</option>
                              <option value="OM">Oman</option>
                              <option value="UG">Ouganda</option>
                              <option value="UZ">Ouzbékistan</option>
                              <option value="PK">Pakistan</option>
                              <option value="PW">Palaos</option>
                              <option value="PA">Panama</option>
                              <option value="PG">Papouasie-Nouvelle-Guinée</option>
                              <option value="PY">Paraguay</option>
                              <option value="NL">Pays-Bas</option>
                              <option value="PH">Philippines</option>
                              <option value="PN">Pitcairn</option>
                              <option value="PL">Pologne</option>
                              <option value="PF">Polynésie française</option>
                              <option value="PR">Porto Rico</option>
                              <option value="PT">Portugal</option>
                              <option value="PE">Pérou</option>
                              <option value="QA">Qatar</option>
                              <option value="RO">Roumanie</option>
                              <option value="GB">Royaume-Uni</option>
                              <option value="RU">Russie</option>
                              <option value="RW">Rwanda</option>
                              <option value="CF">République centrafricaine</option>
                              <option value="DO">République dominicaine</option>
                              <option value="CZ">République tchèque</option>
                              <option value="RE">Réunion</option>
                              <option value="EH">Sahara occidental</option>
                              <option value="BL">Saint-Barthélemy</option>
                              <option value="KN">Saint-Christophe-et-Niévès</option>
                              <option value="SM">Saint-Marin</option>
                              <option value="MF">Saint-Martin (partie française)</option>
                              <option value="PM">Saint-Pierre-et-Miquelon</option>
                              <option value="VC">Saint-Vincent-et-les-Grenadines</option>
                              <option value="SH">Sainte-Hélène</option>
                              <option value="LC">Sainte-Lucie</option>
                              <option value="SV">Salvador</option>
                              <option value="WS">Samoa</option>
                              <option value="AS">Samoa américaines</option>
                              <option value="ST">Sao Tomé-et-Principe</option>
                              <option value="RS">Serbie</option>
                              <option value="SC">Seychelles</option>
                              <option value="SL">Sierra Leone</option>
                              <option value="SG">Singapour</option>
                              <option value="SX">Sint Maarten</option>
                              <option value="SK">Slovaquie</option>
                              <option value="SI">Slovénie</option>
                              <option value="SO">Somalie</option>
                              <option value="SD">Soudan</option>
                              <option value="SS">South Sudan</option>
                              <option value="LK">Sri Lanka</option>
                              <option value="CH">Suisse</option>
                              <option value="SR">Suriname</option>
                              <option value="SE">Suède</option>
                              <option value="SJ">Svalbard et Île Jan Mayen</option>
                              <option value="SZ">Swaziland</option>
                              <option value="SY">Syrie</option>
                              <option value="SN">Sénégal</option>
                              <option value="TJ">Tadjikistan</option>
                              <option value="TZ">Tanzanie</option>
                              <option value="TW">Taïwan</option>
                              <option value="TD">Tchad</option>
                              <option value="TF">Terres australes françaises</option>
                              <option value="IO">Territoire britannique de l'océan Indien</option>
                              <option value="PS">Territoire palestinien</option>
                              <option value="TH">Thaïlande</option>
                              <option value="TL">Timor oriental</option>
                              <option value="TG">Togo</option>
                              <option value="TK">Tokelau</option>
                              <option value="TO">Tonga</option>
                              <option value="TT">Trinité-et-Tobago</option>
                              <option value="TN">Tunisie</option>
                              <option value="TM">Turkménistan</option>
                              <option value="TR">Turquie</option>
                              <option value="TV">Tuvalu</option>
                              <option value="UA">Ukraine</option>
                              <option value="UY">Uruguay</option>
                              <option value="VU">Vanuatu</option>
                              <option value="VA">Vatican</option>
                              <option value="VE">Venezuela</option>
                              <option value="VN">Vietnam</option>
                              <option value="WF">Wallis et Futuna</option>
                              <option value="YE">Yémen</option>
                              <option value="ZM">Zambie</option>
                              <option value="ZW">Zimbabwe</option>
                              <option value="EG">Égypte</option>
                              <option value="AE">Émirats arabes unis</option>
                              <option value="EC">Équateur</option>
                              <option value="ER">Érythrée</option>
                              <option value="US">États-Unis</option>
                              <option value="ET">Éthiopie</option>
                              <option value="BV">Île Bouvet</option>
                              <option value="CX">Île Christmas</option>
                              <option value="IM">Île de Man</option>
                              <option value="NF">Île Norfolk</option>
                              <option value="AX">Îles Aland</option>
                              <option value="KY">Îles Caïmans</option>
                              <option value="CC">Îles Cocos (Keeling)</option>
                              <option value="CK">Îles Cook</option>
                              <option value="FO">Îles Féroé</option>
                              <option value="HM">Îles Heard et MacDonald</option>
                              <option value="FK">Îles Malouines</option>
                              <option value="MP">Îles Mariannes du Nord</option>
                              <option value="MH">Îles Marshall</option>
                              <option value="UM">Îles mineures éloignées des États-Unis</option>
                              <option value="SB">Îles Salomon</option>
                              <option value="TC">Îles Turques-et-Caïques</option>
                              <option value="VI">Îles Vierges américaines</option>
                              <option value="VG">Îles Vierges britanniques</option>
                           </select>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2">
                           <input type="text" name="votre_ville" class="form-control" placeholder="Ville*" required="">
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2">
                           <input name="votre_adresse" rows="4" class="form-control" placeholder="Adresse" required="">
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2">
                           <input type="text" name="votre_cp" class="form-control" placeholder="Code postale*" required="">
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2">
                           <div class="text-center invoice_btn_group">
                              <button id="invoicesubmits2" type="submit" class="btn btn-devis btn-warning">Envoyer</button>
                              <a id="" type="" class="download_invoice">Download Invoice</a>
                           </div>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </form>

               <h4 class="center_border smtitle"><span>Rappel immédiat</span></h4>
               <p class="para-header">Vous souhaitez être rappelé immédiatement ? Laissez nous le numéro où nous pouvons vous joindre, un agent Easyplandetravail va vous rappeler instantanément.</p>
               <form>
               <input type="hidden" name="email" class="email" />
               <table class="col-md-12">
                  <tbody>
                     <tr>
                        <td>
                           <input type="text" name="votre_nom" class="form-control text-control" placeholder="Nom*" required="">
                        </td>
                        <td>
                           <input type="email" name="votre_email" class="form-control mail-control" placeholder="Email*" required="">
                        </td>
                        <td>
                           <input type="text" name="votre_tel" class="form-control text-control" placeholder="Numero de télephone*" required="">
                        </td>
                     </tr>
                     <tr>
                        <td colspan="3" class="text-center">
                           <div><button id="invoicesubmits3" type="submit" class="btn btn-devis btn-warning">Me Rappeler</button></div>
                        </td>
                     </tr>
                  </tbody>
               </table>
            <form>
            </div>

         </div>
         <div class="col-lg-5" style="top: 70%;height:180vh;">
            <div id="booking" class="table_compar table_devis det-devis" style="position: sticky; top: 130px;">
               <div class="table-responsive table-wrapper">
                  <div>
                     <table class="table">
                        <thead>
                           <tr>
                              <th scope="col" colspan="2">
                                 <h4 class="smtitle text-center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Estimate of your project</font></font></h4>
                              </th>
                           </tr>
                        </thead>
                        <tbody id="totaldevis">
                           <tr class="list-det-devis">
                              <td scope="row"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+ Straight edge softened x 0ml</font></font></td>
                              <th class="prix-prod"></th>
                           </tr>
                           <tr class="row-calc bord">
                              <td scope="row">
                                 <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Subtotal HT</font></font>
                                 <h5 id="sub_total_ht"></h5>
                              </td>
                              <th></th>
                           </tr>
                           <tr class="row-calc total-vat">
                              <td scope="row">
                                 <font style="vertical-align: inherit;"><p id="vat_percentage" style="vertical-align: inherit;">20% VAT</p></font>
                                 <h5 id="total_percentage"></h5>
                              </td>
                              <th class="sous-total-prod"></th>
                           </tr>
                           <tr class="row-calc total-devis">
                              <td scope="row">
                                 <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Total order including tax</font></font>
                                 <h5 id="total_res"></h5>
                              </td>
                              <th class="sous-total-prod"></th>
                           </tr>
                           <tr class="row-calc total-devis">
                              <td scope="row">
                                 <button id="send_details" class="btn btn-warning">Receive my detailed quote</button>
                              </td>
                              <th class="sous-total-prod"></th>
                           </tr>

                        </tbody>
                     </table>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-lg-1">
   </div>
</div>
<div class="col-lg-1">
</div>
</div>
<!-- Eviers -->
<div class="modal fade" id="Eviers" tabindex="-1" role="dialog" aria-labelledby="EviersLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="EviersLabel">Sinks</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="container-fluid">
               @foreach($sinks as $sink) 
               <div class="col-lg-4">
                  <div class="card" style="width: 18rem; border-color: black;">
                     <img class="card-img-top" src="{{asset('images/sink/'.$sink->picture)}}" width="100" height="100" alt="Card image cap">
                     <div class="card-body">
                        <h5 class="card-title">{{$sink->model}}</h5>
                        <h5 class="card-title">{{$sink->rate}}</h5>
                        <p class="card-text">{{$sink->size}}</p>
                        <p class="card-text">{{$sink->price}} €</p>
                        <input type="hidden" class="sink_list_value" />
                        <select class="sink_list" id="sink_{{$sink->id}}" data-price="{{$sink->price}}">
                           <option value="0">0</option>
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                           <option value="5">5</option>
                        </select>
                     </div>
                  </div>
               </div>
               @endforeach
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="sink_product_value">Save changes</button>
         </div>
      </div>
   </div>
</div>
<!-- Mitigeurs -->
<div class="modal fade" id="Mitigeurs" tabindex="-1" role="dialog" aria-labelledby="MitigeursLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="MitigeursLabel">Mixer Taps</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="container-fluid">
               @foreach($mixers as $mixer) 
               <div class="col-lg-4">
                  <div class="card" style="width: 18rem; border-color: black;">
                     <img class="card-img-top" src="{{asset('images/mixer/'.$mixer->picture)}}" width="100" height="100" alt="Card image cap">
                     <div class="card-body">
                        <h5 class="card-title">{{$mixer->model}}</h5>
                        <h5 class="card-title">{{$mixer->rate}}</h5>
                        <p class="card-text">{{$mixer->size}}</p>
                        <p class="card-text">{{$mixer->price}} €</p>
                        <input type="hidden" class="mixer_list_value" />
                        <select class="mixer_list" id="mixer_{{$mixer->id}}" data-price="{{$mixer->price}}">
                           <option value="0">0</option>
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                           <option value="5">5</option>
                        </select>
                     </div>
                  </div>
               </div>
               @endforeach
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="mixer_product_value">Save changes</button>
         </div>
      </div>
   </div>
</div>
<!-- Distributeur-Savon -->
<div class="modal fade" id="Distributeur-Savon" tabindex="-1" role="dialog" aria-labelledby="Distributeur-SavonLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="Distributeur-SavonLabel">Soap Dispensers</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="container-fluid">
               @foreach($soaps as $soap) 
               <div class="col-lg-4">
                  <div class="card" style="width: 18rem; border-color: black;">
                     <img class="card-img-top" src="{{asset('images/soap/'.$soap->picture)}}" width="100" height="100" alt="Card image cap">
                     <div class="card-body">
                        <h5 class="card-title">{{$soap->model}}</h5>
                        <h5 class="card-title">{{$soap->rate}}</h5>
                        <p class="card-text">{{$soap->size}}</p>
                        <p class="card-text">{{$soap->price}} €</p>
                        <input type="hidden" class="soap_list_value" />
                        <select class="soap_list" id="soap_{{$soap->id}}" data-price="{{$soap->price}}">
                           <option value="0">0</option>
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                           <option value="5">5</option>
                        </select>
                     </div>
                  </div>
               </div>
               @endforeach
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="soap_product_value">Save changes</button>
         </div>
      </div>
   </div>
</div>
<!-- Panier-égouttoir -->
<div class="modal fade" id="Panier-égouttoir" tabindex="-1" role="dialog" aria-labelledby="Panier-égouttoirLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="Panier-égouttoirLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            ...
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
         </div>
      </div>
   </div>
</div>
@endsection
<script src="{{asset('js/jquery.min.js')}}"></script>
@include('includes.scripts.first')
@include('includes.scripts.third')
@include('includes.scripts.fourth')
@include('includes.scripts.fifth')
@include('includes.scripts.sixth')
@include('includes.scripts.seventh')
@include('includes.scripts.eighth')
@include('includes.scripts.nineth')
<script>
   $(document).on('click', '#send_details', function() {
      var store = $('input[name="store"]:checked').val()
      console.log(store, "store")
      if(store == undefined) {
         alert("Please choose the closest store!");
         return false;
      }
      $('#material_description').hide()
      $('.calculator_area').addClass('hidden')
      $('.customer_area').removeClass('hidden')
      $(this).hide()
      document.body.scrollTop = 0; // For Safari
      document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      $(".container-fluid").scrollTop()
   })

   // $(document).on('click', '#invoicesubmits', function() {
   //    price_list = [];
   //    $('#totaldevis > tr').each(function() {
   //       temp = []
   //       var name = $(this).attr('data-name');
   //       var price = $(this).attr('data-price');
   //       temp.push(name)
   //       temp.push(price)
   //       price_list.push(temp)
   //       temp = []
   //    });
   //    console.log(price_list,"price list")
   // })
   $(document).on('click', '#invoicesubmits2', function() {
      price_list = [];
      $('#totaldevis > tr').each(function() {
         temp = []
         var name = $(this).attr('data-name');
         var price = $(this).attr('data-price');
         if(name != undefined) {
            temp.push(name)
            temp.push(price)
            price_list.push(temp)
            temp = []
         }
      });
      console.log(price_list,"price list")
      var data = [];

      var sub_total_ht  = $("#sub_total_ht").text()
      var vat   = $("#total_percentage").text()
      var total = $("#total_res").text()
      
      data = {'price_list': price_list, 'total':total, 'vat': vat, 'sub_total_ht':sub_total_ht}
      
      $.ajax({
         method:'POST',
         url:'/sendInvoice',
         data:{data: data,"_token":"{{csrf_token()}}"},
         success: function(res) {
            const linkSource = `data:application/pdf;base64, `+res;
            // const downloadLink = document.createElement("a");
            // const fileName = "vct_illustration.pdf";

            // downloadLink.href = linkSource;
            // downloadLink.download = fileName;
            var download_btn = $(".download_invoice")
            download_btn.show()
            download_btn.attr('href', linkSource)
            download_btn.attr('download', 'invoice.pdf')
            
            // downl,oadLink.click();

         }
      })
   })
</script>