<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bordure extends Model
{
    public function matériaus()
    {
        return $this->belongsTo('App\Matériau','material_id');
    }

	    public function coloris()
    {
        return $this->belongsTo('App\Coloris','coloris_id');
    }
}
