<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pagesettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagesettings', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('titlecolor');
            $table->string('logo');
            $table->string('headerimg');
            $table->string('footerimg');
            $table->string('coveragetitle');
            $table->string('coveragecontent');
            $table->timestamps();

        });
    }

    /**
     * 
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
